function createStringWith1and0(numbOfOne) {
	let newString = [];
	for ( let i = 0 ; i < 8 ; i++ ) {
		if ( i < numbOfOne ) newString[i] = "1";
		else newString[i] = "0";
	}
	newString = newString.join("");
	return newString;
}

function createArrayMaskBin(mask) {
	let maskBin = [];
	const numbOfOneIneLastString = mask % 8;
	const numbOfStringFullOf1 = (mask - numbOfOneIneLastString) / 8;

	for ( let i = 0 ; i < 4 ; i++ ) {
		if ( i < numbOfStringFullOf1 ) {
			maskBin[i] = "11111111";
		} else if ( i === numbOfStringFullOf1) {
			maskBin[i] = createStringWith1and0(numbOfOneIneLastString);
		} else {
			maskBin[i] = "00000000";
		}
	}

	return maskBin;
}

export default createArrayMaskBin;