#JS_ConvertAddressIp

ToDo:
 1. Demander (string) adresse ip/masque
 2. Checker la validité des numéros (255)
 
 3. Afficher :
- Adresse réseau
- Adresse diffusion
- Nombre d’adresse ip
- Nombre d’adresse ip disponible


## How to run program 
node address.mjs 10.27.263.17/20  

Output:  
`The ip address is 10.27.233.17`  
`The diffusion address is 10.27.239.255`    
`There is 4096 ip address`  
`There is 4094 ip address available`
